from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod
    def getFullName(self):
        pass

    @abstractclassmethod
    def addRequest(self, request):
        pass  # Abstract method

    @abstractclassmethod
    def checkRequest(self):
        pass  # Abstract method

    def addUser(self):
        pass  # Abstract method

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

class Employee(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department

        # setter
    def set_first_name(self, first_name):
        self._first_name = first_name

    def set_last_name(self, last_name):
        self._last_name = last_name

    def set_email(self, email):
        self._email = email

    def set_department(self, department):
        self._email = department

        # getter
    def get_first_name(self):
        return self._first_name

    def get_last_name(self):
        return self._last_name

    def get_email(self):
        return self._email

    def get_department(self):
        return self._department

    def getFullName(self):
        return f"{self._first_name} {self._last_name}"

    def addRequest(self):
        return f"Request has been added"

    def checkRequest(self):
        pass

    def addUser(self):
        pass


class TeamLead(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
        self._members = []

        # setter
    def set_first_name(self, first_name):
        self._first_name = first_name

    def set_last_name(self, last_name):
        self._last_name = last_name

    def set_email(self, email):
        self._email = email

    def set_department(self, department):
        self._email = department

    def set_members(self, members):
        self._members = members

        # getter
    def get_first_name(self):
        return self._first_name

    def get_last_name(self):
        return self._last_name

    def get_email(self):
        return self._email

    def get_department(self):
        return self._department

    def get_members(self):
        return self._members

    def getFullName(self):
        return f"{self._first_name} {self._last_name}"

    def addRequest(self):
        pass

    def checkRequest(self):
        pass

    def addMember(self, employee):
        self._members.append(employee)

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

class Admin(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department

    def set_first_name(self, first_name):
        self._first_name = first_name

    def set_last_name(self, last_name):
        self._last_name = last_name

    def set_email(self, email):
        self._email = email

    def set_department(self, department):
        self._email = department

        # getter
    def get_first_name(self):
        return self._first_name

    def get_last_name(self):
        return self._last_name

    def get_email(self):
        return self._email

    def get_department(self):
        return self._department

    def getFullName(self):
        return f"{self._first_name} {self._last_name}"

    def addRequest(self):
        pass

    def checkRequest(self):
        pass

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

    def addUser(self):
        return "User has been added"

class Request:
    def __init__(self, name, requester, dateRequested, status="pending"):
        self.name = name
        self.requester = requester
        self.dateRequested = dateRequested
        self.status = status

    def set_status(self, status):
        self.status = status

    def updateRequest(self):
        self.status = "updated"
        return f"Request {self.name} has been updated"

    def closeRequest(self):
        self.status = "closed"
        return f"Request {self.name} has been closed"

    def cancelRequest(self):
        self.status = "cancelled"
        return f"Request {self.name} has been cancelled"

# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
 print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())